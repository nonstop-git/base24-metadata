#NSGit BASE24 Shared Structure#

This Metadata describes how to manage BASE24 in an NSGIT/git environment.

Copy or modify the directory structure as you see fit, but make sure the
mappings are maintained properly. You can test your mappings using various
NSGIT commands including:

```
	NSGIT subvolume-map test ...
	NSGIT nsk-map test ...
```

See the online help for each.

##Limitations##

Copying of this project is permitted. However, you should consider doing this
via a GIT FORK process. When you have made changes, check them back into your
fork, and inform the maintainer via PULL REQUEST to pick up your changes so
that BASE24 can be managed consistently across environments.
